import React from 'react'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

function Box (props) {

  return (
    <div style={{ width: '300px', marginBottom: '15px' }}>
      <Card>
        <CardActionArea>
          <CardMedia
            component="img"
            alt="Contemplative Reptile"
            height="140"
            image={props.image}
            title="Contemplative Reptile"
          />
          <CardContent>
            <Typography style={{ color: 'white' }} gutterBottom variant="h5" component="h2">
              {props.title}
          </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              Aprende a usar {props.title} con este tutorial
          </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small" color="primary">
            Compartir
        </Button>
          <Button size="small" color="primary">
            Aprender
        </Button>
        </CardActions>
      </Card>
    </div>
  )
}

export default Box